package com.example.apilist.database.database

import android.app.Application
import androidx.room.Room

class CountryApplication: Application() {
    companion object {
        lateinit var database: CountryDatabase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this, CountryDatabase::class.java, "CountryDatabase").build()
    }
}