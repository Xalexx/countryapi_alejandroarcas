package com.example.apilist.database.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "CountryFavouriteEntity")
data class CountryFavouriteEntity(
    @PrimaryKey var countryName: String,
    var countryFlag: String,
    var countryOfficialName: String,
    var countryPopulation: String
)
