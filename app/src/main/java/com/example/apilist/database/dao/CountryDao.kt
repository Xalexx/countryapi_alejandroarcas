package com.example.apilist.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.apilist.database.database.model.CountryFavouriteEntity

@Dao
interface CountryDao {
    @Query("SELECT * FROM CountryFavouriteEntity")
    fun getAllCountries(): MutableList<CountryFavouriteEntity>
    @Query("SELECT * FROM CountryFavouriteEntity WHERE countryName = :specificName")
    fun getSpecificCountry(specificName: String): MutableList<CountryFavouriteEntity>
    @Insert
    fun addCountry(country: CountryFavouriteEntity)
    @Delete
    fun removeCountry(country: CountryFavouriteEntity)
    @Query("SELECT count(*) FROM CountryFavouriteEntity WHERE countryName = :name")
    fun existInDataBase(name: String): Int

}