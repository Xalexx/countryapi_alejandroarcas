package com.example.apilist.database.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.apilist.database.dao.CountryDao
import com.example.apilist.database.database.model.CountryFavouriteEntity

@Database(entities = [CountryFavouriteEntity::class], version = 1)
abstract class CountryDatabase: RoomDatabase() {
    abstract fun countryDao(): CountryDao
}