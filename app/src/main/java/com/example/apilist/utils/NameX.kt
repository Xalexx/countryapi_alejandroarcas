package com.example.apilist.utils

data class NameX(
    val common: String,
    val official: String
)