package com.example.apilist.utils

data class PostalCodeX(
    val format: String,
    val regex: String
)