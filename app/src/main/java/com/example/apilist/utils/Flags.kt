package com.example.apilist.utils

data class Flags(
    val alt: String,
    val png: String,
    val svg: String
)