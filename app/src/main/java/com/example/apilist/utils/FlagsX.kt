package com.example.apilist.utils

data class FlagsX(
    val alt: String,
    val png: String,
    val svg: String
)