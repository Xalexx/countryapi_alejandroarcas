package com.example.apilist.utils

data class PostalCode(
    val format: String,
    val regex: String
)