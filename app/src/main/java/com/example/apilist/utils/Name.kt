package com.example.apilist.utils

data class Name(
    val common: String,
    val official: String
)