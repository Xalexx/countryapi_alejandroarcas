package com.example.apilist.utils

data class CapitalInfo(
    val latlng: List<Double>
)