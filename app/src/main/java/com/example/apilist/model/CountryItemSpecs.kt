package com.example.apilist.model

import com.example.apilist.utils.FlagsX
import com.example.apilist.utils.Name
import com.example.apilist.utils.PostalCodeX

data class CountryItemSpecs(
    val altSpellings: List<String>,
    val area: Double,
    val borders: List<String>,
    val capital: List<String>,
    val continents: List<String>,
    val name: Name,
    val flag: String,
    val flags: FlagsX,
    val population: Int,
    val postalCode: PostalCodeX,
    val region: String,
    val startOfWeek: String,
    val subregion: String
)