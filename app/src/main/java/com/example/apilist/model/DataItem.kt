package com.example.apilist.model

import com.example.apilist.utils.Flags
import com.example.apilist.utils.Name
import com.example.apilist.utils.PostalCode

data class DataItem(
    val altSpellings: List<String>,
    val area: Double,
    val borders: List<String>,
    val capital: List<String>,
    val continents: List<String>,
    val flags: Flags,
    val name: Name,
    val population: Int,
    val postalCode: PostalCode,
    val region: String,
    val startOfWeek: String,
    val status: String,
    val subregion: String,
    val timezones: List<String>,
    val tld: List<String>,
    val unMember: Boolean
)