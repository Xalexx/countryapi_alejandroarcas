package com.example.apilist.view

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.apilist.viewmodel.QuotesTagsViewModel
import com.example.apilist.R
import com.example.apilist.databinding.FragmentDetailBinding
import com.example.apilist.database.database.CountryApplication
import com.example.apilist.database.database.model.CountryFavouriteEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val viewModel = ViewModelProvider(requireActivity())[QuotesTagsViewModel::class.java]
        val name = arguments?.getString("name").toString()
        viewModel.fetchCountry(name)
        viewModel.dataName.observe(viewLifecycleOwner) {



            val officialName = it?.get(0)?.name?.official
            val population = it?.get(0)?.population.toString()
            val continent = it?.get(0)?.continents?.get(0)
            val area = it?.get(0)?.area.toString()
            val capital = it?.get(0)?.capital?.get(0)
            val flag = it?.get(0)?.flags?.png

            binding.countryTitleTextView.visibility = View.VISIBLE
            binding.countryOfficialNameDetailTextView.visibility = View.VISIBLE
            binding.populationTextView.visibility = View.VISIBLE
            binding.populationImageView.visibility = View.VISIBLE
            binding.areaTextView.visibility = View.VISIBLE
            binding.areaImageView.visibility = View.VISIBLE
            binding.capitalTextView.visibility = View.VISIBLE
            binding.capitalImageView.visibility = View.VISIBLE
            binding.continentTextView.visibility = View.VISIBLE
            binding.postalCodeImageView.visibility = View.VISIBLE
            binding.favouriteButtonImageView.visibility = View.VISIBLE
            binding.capitalImageView.visibility = View.VISIBLE
            binding.capitalTextView.visibility = View.VISIBLE
            binding.countryFlagDetailImageView.visibility = View.VISIBLE
            binding.weekImageView.visibility = View.VISIBLE
            binding.weekTextView.visibility = View.VISIBLE

            binding.progressBarDetail.visibility = View.GONE
            binding.loadingDataTextView.visibility = View.GONE


            binding.countryTitleTextView.text = name
            binding.countryOfficialNameDetailTextView.text = officialName
            binding.populationTextView.text = population
            binding.continentTextView.text = continent
            binding.areaTextView.text = area
            binding.capitalTextView.text = capital

            var imageState = 2

            CoroutineScope(Dispatchers.IO).launch {
                imageState = if (CountryApplication.database.countryDao().existInDataBase(name) == 1){
                    binding.favouriteButtonImageView.setImageResource(R.drawable.favourite_on)
                    1
                } else {
                    binding.favouriteButtonImageView.setImageResource(R.drawable.favourite_off)
                    0
                }
            }

            Glide.with(requireActivity())
                .load(flag)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.countryFlagDetailImageView)

            binding.favouriteButtonImageView.setOnClickListener {
                imageState = if (imageState == 1) {
                    CoroutineScope(Dispatchers.IO).launch {
                        CountryApplication.database.countryDao().removeCountry(
                            CountryFavouriteEntity(name, flag!!, officialName!!, population)
                        )
                    }
                    binding.favouriteButtonImageView.setImageResource(R.drawable.favourite_off)
                    0
                } else {
                    val newCountryInFavourite = CountryFavouriteEntity(name, flag!!, officialName!!, population)
                    CoroutineScope(Dispatchers.IO).launch {
                        CountryApplication.database.countryDao().addCountry(newCountryInFavourite)
                    }
                    binding.favouriteButtonImageView.setImageResource(R.drawable.favourite_on)
                    1
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        binding.countryTitleTextView.visibility = View.GONE
        binding.countryOfficialNameDetailTextView.visibility = View.GONE
        binding.populationTextView.visibility = View.GONE
        binding.populationImageView.visibility = View.GONE
        binding.areaTextView.visibility = View.GONE
        binding.areaImageView.visibility = View.GONE
        binding.capitalTextView.visibility = View.GONE
        binding.capitalImageView.visibility = View.GONE
        binding.continentTextView.visibility = View.GONE
        binding.postalCodeImageView.visibility = View.GONE
        binding.favouriteButtonImageView.visibility = View.GONE
        binding.capitalImageView.visibility = View.GONE
        binding.capitalTextView.visibility = View.GONE
        binding.countryFlagDetailImageView.visibility = View.GONE
        binding.weekImageView.visibility = View.GONE
        binding.weekTextView.visibility = View.GONE

        binding.progressBarDetail.visibility = View.VISIBLE
        binding.loadingDataTextView.visibility = View.VISIBLE
    }
}