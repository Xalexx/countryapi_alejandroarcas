package com.example.apilist.view

import android.os.Bundle
import android.view.*
import android.widget.SearchView.OnQueryTextListener
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apilist.recyclerview.interfaces.OnClickListener
import com.example.apilist.databinding.FragmentFavouriteBinding
import com.example.apilist.database.database.CountryApplication
import com.example.apilist.database.database.model.CountryFavouriteEntity
import com.example.apilist.model.DataItem
import com.example.apilist.recyclerview.adapter.FavouriteAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class FavouriteFragment : Fragment(), OnClickListener {

    private lateinit var favouriteAdapter: FavouriteAdapter
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentFavouriteBinding
    private lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavouriteBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        CoroutineScope(Dispatchers.IO).launch {
            val list = CountryApplication.database.countryDao().getAllCountries()
            withContext(Dispatchers.Main){
                setUpRecyclerViewFavourite(list)
                searchView = binding.searchView
                searchView.clearFocus()

                searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    filterList(newText)
                    return true
                }

                })
            }
        }
    }

    private fun filterList(query: String?) {
        if (query != null) {
            val filteredList = mutableListOf<CountryFavouriteEntity>()
            CoroutineScope(Dispatchers.IO).launch{
                val listFilter = CountryApplication.database.countryDao().getAllCountries()
                println(listFilter)
                for (i in listFilter.indices){
                    if (listFilter[i].countryName.lowercase(Locale.ROOT).contains(query.lowercase())){
                        filteredList.add(listFilter[i])
                    }
                }
                withContext(Dispatchers.Main){
                    if (filteredList.size == 0) {
                        Toast.makeText(context, "No Country Found", Toast.LENGTH_SHORT).show()
                    } else {
                        favouriteAdapter.setFilteredList(filteredList)
                    }
                }
            }
        }
    }

    // OnClickListener

    override fun onClick(country: DataItem) {
        // List Fragment
    }

    override fun onClickFavourite(country: CountryFavouriteEntity) {
        // List Fragment
    }

    override fun onClickDelete(country: CountryFavouriteEntity) {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) { CountryApplication.database.countryDao().removeCountry(country) }
            favouriteAdapter.delete(country)
        }
    }

    private fun setUpRecyclerViewFavourite(list: MutableList<CountryFavouriteEntity>) {
        favouriteAdapter = FavouriteAdapter(list, this)
        mLayoutManager = LinearLayoutManager(context)

        binding.recyclerFavourite.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManager
            adapter = favouriteAdapter
        }
    }

}

