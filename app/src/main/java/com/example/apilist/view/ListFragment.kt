package com.example.apilist.view

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apilist.database.database.CountryApplication
import com.example.apilist.recyclerview.adapter.CountryAdapter
import com.example.apilist.database.database.model.CountryFavouriteEntity
import com.example.apilist.recyclerview.interfaces.OnClickListener
import com.example.apilist.databinding.FragmentListBinding
import com.example.apilist.model.*
import com.example.apilist.viewmodel.QuotesTagsViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList

class ListFragment : Fragment(), OnClickListener {

    private lateinit var countryAdapter: CountryAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentListBinding
    private lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProvider(requireActivity())[QuotesTagsViewModel::class.java]
        viewModel.data.observe(viewLifecycleOwner){
            binding.searchView.visibility = View.VISIBLE
            binding.progressBarList.visibility = View.GONE
            binding.loadingDataTextView.visibility = View.GONE
            setUpRecyclerView(it!!)


            fun filterList(query: String?) {
                if (query != null) {
                    val filteredList = ArrayList<DataItem>()
                    for (i in viewModel.data.value!!){
                        if (i.name.common.lowercase(Locale.ROOT).contains(query.lowercase())){
                            filteredList.add(i)
                        }
                        println(filteredList)
                    }
                    if (filteredList.size == 0) {
                        Toast.makeText(context, "No Country Found", Toast.LENGTH_SHORT).show()
                    } else {
                        countryAdapter.setFilteredList(filteredList)
                    }

                }
            }


            searchView = binding.searchView
            searchView.clearFocus()

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    filterList(newText)
                    return true
                }

            })
        }

    }


    private fun setUpRecyclerView(list: ArrayList<DataItem>) {
        countryAdapter = CountryAdapter(list, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = countryAdapter
        }
    }

    override fun onClick(country: DataItem) {
        val action = ListFragmentDirections.actionListFragmentToDetailFragment(country.name.common)
        findNavController().navigate(action)
    }

    override fun onClickFavourite(country: CountryFavouriteEntity) {
        // Favourite fragment
    }

    override fun onClickDelete(country: CountryFavouriteEntity) {
        // Favourite Fragment
    }

    override fun onResume() {
        super.onResume()
        binding.progressBarList.visibility = View.VISIBLE
        binding.loadingDataTextView.visibility = View.VISIBLE
    }
}