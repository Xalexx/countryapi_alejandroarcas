package com.example.apilist.recyclerview.interfaces

import com.example.apilist.database.database.model.CountryFavouriteEntity
import com.example.apilist.model.DataItem

interface OnClickListener {
    fun onClick(country: DataItem)

    fun onClickFavourite(country: CountryFavouriteEntity)

    fun onClickDelete(country: CountryFavouriteEntity)
}