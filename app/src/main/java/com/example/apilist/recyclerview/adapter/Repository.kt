package com.example.apilist.recyclerview.adapter

import com.example.apilist.recyclerview.interfaces.CountryInterfaceApi

class Repository {
    val apiInterface = CountryInterfaceApi.create()

    suspend fun getCountries() = apiInterface.getCountries()
    suspend fun getCountry(name: String) = apiInterface.getCountry(name)
}