package com.example.apilist.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.apilist.R
import com.example.apilist.database.database.model.CountryFavouriteEntity
import com.example.apilist.databinding.CountryItemBinding
import com.example.apilist.model.DataItem
import com.example.apilist.recyclerview.interfaces.OnClickListener

class CountryAdapter(private var countries: ArrayList<DataItem>, private val listener: OnClickListener) :
    RecyclerView.Adapter<CountryAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = CountryItemBinding.bind(view)
        fun setListener(country: DataItem){
            binding.root.setOnClickListener {
                listener.onClick(country)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.country_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return countries.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val country = countries[position]
        with(holder){
            setListener(country)
            binding.countryNameTextView.text = country.name.common
            binding.countryOfficialNameTextView.text = country.name.official
            Glide.with(context)
                .load(country.flags.png)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.countryFlagImageView)
        }
    }

    fun setFilteredList(countryListItems: ArrayList<DataItem>){
        countries = countryListItems
        notifyDataSetChanged()
    }
}