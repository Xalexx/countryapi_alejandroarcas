package com.example.apilist.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.apilist.recyclerview.interfaces.OnClickListener
import com.example.apilist.R
import com.example.apilist.database.database.model.CountryFavouriteEntity
import com.example.apilist.databinding.CountryItemFavouriteBinding

class FavouriteAdapter(private var countries: MutableList<CountryFavouriteEntity>,
                       private var listener: OnClickListener
): RecyclerView.Adapter<FavouriteAdapter.ViewHolder>() {

        private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = CountryItemFavouriteBinding.bind(view)

        fun setListener(country: CountryFavouriteEntity){
            binding.root.setOnClickListener {
                listener.onClickFavourite(country)
            }
            binding.trashButtonButton.setOnClickListener {
                listener.onClickDelete(country)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.country_item_favourite, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return countries.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val country = countries[position]
        with(holder){
            setListener(country)
            binding.countryNameFavTextView.text = country.countryName
            binding.countryOfficialNameFavTextView.text = "Official Name: ${country.countryOfficialName}"
            binding.countryPopulationFavTextView.text = "Population: ${country.countryPopulation}"

            Glide.with(context)
                .load(country.countryFlag)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.countryFlagFavImageView)
        }
    }

    fun delete(country: CountryFavouriteEntity) {
        val index = countries.indexOf(country)
        if(index != -1){
            countries.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    fun setFilteredList(countryListFavouriteFiltered: MutableList<CountryFavouriteEntity>){
        countries = countryListFavouriteFiltered
        notifyDataSetChanged()
    }


}
