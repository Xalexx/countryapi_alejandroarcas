package com.example.apilist.recyclerview.interfaces

import com.example.apilist.model.CountryItem
import com.example.apilist.model.Data
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface CountryInterfaceApi {
    @GET("all")
    suspend fun getCountries(): Response<Data>

    @GET("name/{name}")
    suspend fun getCountry(@Path("name") countryName: String): Response<CountryItem>

    companion object {
        private const val BASE_URL = "https://restcountries.com/v3.1/"
        fun create(): CountryInterfaceApi {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(CountryInterfaceApi::class.java)
        }
    }

    }