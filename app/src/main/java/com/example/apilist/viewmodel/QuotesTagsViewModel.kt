package com.example.apilist.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.apilist.recyclerview.adapter.Repository
import com.example.apilist.model.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class QuotesTagsViewModel : ViewModel() {
    val repository = Repository()
    var data = MutableLiveData<Data?>()
    var dataName = MutableLiveData<CountryItem?>()

    init {
        fetchData()
    }

    private fun fetchData() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCountries()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    data.postValue(response.body())
                } else {
                    println("Error :, ${response.message()}")
                }
            }
        }
    }

    fun fetchCountry(name: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCountry(name)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    dataName.postValue(response.body())
                } else {
                    println("Error :, ${response.message()}")
                }
            }
        }
    }

}